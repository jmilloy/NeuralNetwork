''' Activation Functions for Neural Networks '''

import math

#linear
class step:
    def __init__(self, threshold):
        self.threshold = threshold
    
    def f(self, x):
        return int(x>=self.threshold)



#nonlinear/sigmoid
class tanh:
    def __init__(self, b):
        self.b = b
    
    def f(self,x):
        ''' activation function, b->0 decreases slope and aids interpolation, range is (-1,1) '''
        return math.tanh(2*self.b*x)
        
    def df(self,x):
        ''' b * sech^2(bx) '''
        return self.b * 4.0 * pow(math.cosh(self.b*x), 2) / pow(math.cosh(2*self.b*x) + 1, 2)

class logistic:
    def __init__(self, b):
        self.b = b
    
    def f(self, x):
        ''' logistic function, b->0 decreases slope and aids interpolation, range is (0,1) '''
        if self.b * x < -50:
            return 0
        else:
            return 1.0 / (math.e**(-2.0 * self.b * x) + 1.0)
    
    def df(self, x):
        fx = self.f(x)
        return fx*(1-fx)

