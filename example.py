
import ActivationFunction
from NeuralNet import Perceptron
from NeuralNet import BackProp2
    
#choose an activation function
fn = ActivationFunction.step(10)
fn = ActivationFunction.tanh(0.1)
fn = ActivationFunction.logistic(0.1)
    
#create randomized perceptron or backpropagation network
inputs = ['age', 'weight', 'height']
nn = Perceptron(inputs, fn)
nn = BackProp2(inputs, 6, activation_fn)
    
#train, display, and evaluate the network
nn.train(training_data, 500, 0.05)
nn.display()
print nn.mse(test_data)
    
#use the network
x = test_data[0]
print nn.score(x)
print nn.classify(x)
    
#save and load networks from file
nn.save('net.nn')
nn.load('net.nn')
