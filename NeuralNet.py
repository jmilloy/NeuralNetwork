import random
import math
import sys

class GenericNetwork:
    def __init__(self, inputs):
        if type(inputs) is list:
            self.inputs = inputs
            self.num_inputs = len(inputs)
        elif type(inputs) is int:
            self.inputs = range(inputs)
            self.num_inputs = inputs
    
    def classify(self, x):
        return self.f(self.score(x))

    def mse(self, data):
        ''' data: list of (x,t) pairs '''
        MSE = 0
        for (x,t) in data:
            e = t - self.classify(x['data'])
            MSE += 0.5 * e**2
        return MSE
        
    def train(self, data, n, k, progress=False):
        ''' data: list of (x,t) pairs, n: number of epochs, k: learning rate '''
        if progress:
            sys.stderr.write("TRAINING < %s > %d EPOCHS\rTRAINING < " % (''.join(['.']*100), n))
            sys.stderr.flush()
        
        for epoch in range(n):
            #adjust weights for each data point
            for (x,t) in data:
                self.learn(x['data'],t,k)
            
            #shuffle the data
            random.shuffle(data)
            
            if progress and epoch % (n/100) == 0:
                sys.stderr.write("=") #\r%d%%" %i)    # or print >> sys.stdout, "\r%d%%" %i,
                sys.stderr.flush()
        
        if progress:
            sys.stderr.write(" >\n")
            sys.stderr.flush()
    
    def train_history(self, data, n, k, progress=False, error=0):
        ''' record and return MSE history '''
        MSE = [0]*n
        for epoch in range(n):
            #adjust the weights for each data point
            for (x,t) in data:
                e = self.learn(x['data'],t,k)
                MSE[epoch] += 0.5 * e**2
            
            #shuffle the data
            random.shuffle(data)
            
            if progress and epoch % (n/10) == 0:
                sys.stderr.write("\t%1d%%: %0.2f MSE\n" % (int(100*float(epoch)/n),MSE[epoch]))
                sys.stderr.flush()
            
            if MSE[epoch] < error:
                break
        
        if progress:
            sys.stderr.write("\t100%%: %0.2f MSE\n" % (MSE[epoch]))
            sys.stderr.flush()
    
        return MSE
    
    def rank(self, data):
        ''' data: list of PSMs '''
        return sorted([(x, self.score(x['data'])) for x in data], key=lambda (x,score): -score)


class Perceptron(GenericNetwork):
    def __init__(self, inputs, activation_fn):
        GenericNetwork.__init__(self, inputs)
        self.f = activation_fn.f
        self.activation_fn = type(activation_fn)
        
        #weight matrix (synapses)
        self.W = [0.0]*self.num_inputs
        for i in range(self.num_inputs):
            self.W[i] = 2*(random.random()-0.5)
    
    def type_code(self):
        return 'p'
    
    def score(self, x):
        ''' net performance rule '''
        return sum([x[i] * self.W[i] for i in range(self.num_inputs)])
    
    def learn(self, x, t, k):
        ''' single layer delta rule '''
        y = self.f(self.score(x))
        for i in range(self.num_inputs):
            self.W[i] += x[i] * (t-y) * k
        
        return t-y
    
    def display(self):
        for i,key in enumerate(self.inputs):
            s = "%.4f" % round(self.W[i],2)
            print key.ljust(16),s.rjust(7),"\t"
    
    def save(self, f):
        print >> f, self.num_inputs
        print >> f, ",".join(self.inputs)
        print >> f, ",".join(["%f" % w for w in self.W])
    
    def load(self, f):
        self.num_inputs = int(f.readline())
        self.inputs = f.readline().strip().split(',')
        self.W = map(float,f.readline().split(','))


class BackProp2(GenericNetwork):
    def __init__(self, inputs, num_hidden_nodes, activation_fn):
        GenericNetwork.__init__(self, inputs)
        
        self.h = num_hidden_nodes
        self.f = activation_fn.f
        self.df = activation_fn.df
        
        #hidden layer A (hidden x input)
        self.A = [None]*self.h
        for j in range(self.h):
            self.A[j] = [None]*self.num_inputs
            for i in range(self.num_inputs):
                self.A[j][i] = 2*(random.random()-0.5)
        
        #output layer B (1 x hidden)
        self.B = [None]*self.h
        for i in range(self.h):
            self.B[i] = 2*(random.random()-0.5)
    
    def type_code(self):
        return 'bp2'
    
    def hidden(self,x):
        ''' hidden signal q = f(Ax) '''
        q = [None]*self.h
        for j in range(self.h):
            net = sum([x[i] * self.A[j][i] for i in range(self.num_inputs)])
            q[j] = self.f(net)
        return q
    
    def score(self, x):
        ''' net performance rule B(f(Ax))'''
        q = self.hidden(x)
        return sum(q[j] * self.B[j] for j in range(self.h))
    
    def learn(self, x, t, k):
        ''' on-line backpropagation'''
        
        #compute q=f(Ax), nety=Bq, y=f(nety)
        q = self.hidden(x)
        nety = sum([q[j] * self.B[j] for j in range(self.h)])
        y = self.f(nety)
        
        #compute error in output layer B using the derivative of f and the error t-y
        delta_B = self.df(y) * (t-y)
        
        #adjust B
        for j in range(self.h):
            self.B[j] += k * delta_B * q[j]
        
        #backpropagate to A
        for j in range(self.h):
            #compute error in hidden layer A using the derivative of f and sum of the weighted delta_B (there is only one)
            delta_A = self.df(q[j]) * delta_B * self.B[j]
            for i in range(self.num_inputs):
                self.A[j][i] += k * delta_A * x[i]
        
        return y-t
    
    def display(self):
        #hidden layer A
        for i,key in enumerate(self.inputs):
            print key.ljust(16),
            for j in range(self.h):
                s = "%.4f" % round(self.A[j][i],2)
                print s.rjust(7),"\t",
            print
        
        #output layer B
        print
        print ' '*16,
        for j in range(self.h):
            s = "%.4f" % round(self.B[j],2)
            print s.rjust(7),"\t",
        print
    
    def save(self, f):
        print >> f, self.num_inputs
        print >> f, ",".join(self.inputs)
        print >> f, self.h
        for j in range(self.h):
            print >> f, ",".join(["%f" % a for a in self.A[j]])
        print >> f, ",".join(["%f" % b for b in self.B])
    
    def load(self, f):
        self.num_inputs = int(f.readline())
        self.inputs = f.readline().strip().split(',')
        self.h = int(f.readline())
        self.A = [None]*self.h
        for j in range(self.h):
            self.A[j] = map(float,f.readline().split(','))
        self.B = map(float,f.readline().split(','))

